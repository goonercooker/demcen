const Discord = require('discord.io');
const logger = require('winston');
const auth = require('./auth.json');
var heroflag = true;
var hero = '';
var gulag = [];
var members = [];
const serverid = '574963795517243403';
const gulagid = '604344252918399015';
const heroid = '613778056791982092';
const herochannel = '619246652221292582';
const gulagchannel = '604344763654602772';
const roles = ['612304645389549579', '610195397142642697', '613778056791982092', '574967118211055637', '574966797468565525', '622511650469183488', '584449954953691138', '574966182818349076']
const roasters = [" will one day be showered with accolades, while writing an autobiography!", 
                " your content is so Sanghi that surely your birth certificate was an apology letter from Veer Savarkar!", 
                " you speak so much shit that one starts wondering whether your mom conceived you or forwarded you on whatsapp!", 
                " your existence is like the iphone or Venezuela argument against socialism, overused and meaningless!",
                " are you men's right activism? Because you always pretend like you have a problem and yet nobody takes you seriously!",
                " your views are so irrelevant that even Arnab will let you speak on his show!", 
                " you receive as much love and respect from the society as Advani receives from Modi!", 
                " one day you will find your significant other, get married and then spend your entire life like Jashodaben Narendrabhai Modi", 
                " you deserve a lot \n\n\nworse than Kulaks", 
                " all your friends are like an individual's merit, you think you have them but they are just in your head!",
                " you are so cringeworthy, that in order to praise you, your mom needs to brush up her acting skills more than Modiji on October the 2nd.", 
                " you are so irrelevant that Amit Shah refuses to stalk you.", 
                " your profile picture makes you look like you are about to drop the 'human nature' argument against Marxism any second now.",
                " there's Bhagat Singh who at 23, sacrificed his life for creating a socialist India and then there's you who at 23, believed that Ajay Devgan was the actual Bhagat Singh"]
// Configure logger settings
logger.remove(logger.transports.Console);
logger.add(new logger.transports.Console, {
    colorize: true
});
logger.level = 'debug';
// Initialize Discord Bot
var bot = new Discord.Client({
   token: auth.token,
   autorun: true
});
bot.on('ready', function (evt) {
    logger.info('Connected');
    logger.info('Logged in as: ');
    logger.info(bot.username + ' - (' + bot.id + ')');
    bot.getMembers({
        limit: 500,
        serverID: serverid
    }, (err, users) => {
        if (err) console.log(err);
        else {
            members = users;
            console.log(users.length + ' users on the server!');
            for (let i=0; i<users.length; i++) {
                if(users[i].roles.includes(heroid)){
                    hero = users[i].user.id;
                    console.log('Current Hero of the Server - ' + users[i].user.username)
                    break;
                }
            }
            users.forEach((member) => {
                if(member.roles.includes(gulagid)){
                    var user = {}
                    user.id = member.user.id;
                    user.removed = [];
                    user.removed.push('622511650469183488');
                    gulag.push(user);
                }
            })
            console.log('Gulag inmates - ' + gulag)
            // bot.getMessage({
            //     channelID: '600988117247328270',
            //     messageID: '653614266070466590'
            // }, (err, msg) => {
            //     if (err) console.log(err);
            //     else console.log(msg);
            // });
            // user = '483125893137694731'
            // bot.sendMessage({
            //     to: '600988117247328270',
            //     message: "<@" + user + "> is reading Kant these days. Now, he believes in categorical imperative instead of FoS" 
            // })
        }
    })
});
bot.on('message', function (user, userID, channelID, message, evt) {
    // Our bot needs to know if it will execute a command
    // It will listen for messages that will start with `=`
    if (message.substring(0, 1) == '=') {
        var args = message.substring(1).split(' ');
        var cmd = args[0];
        var name = args[1];
        var reason = args.slice(2, args.length).join(' ');
        var misc = args.slice(1, args.length).join(' ');
        var miscarr = misc.split('');
        if (miscarr.length) miscarr[0] = miscarr[0].toUpperCase();
        if (miscarr[miscarr.length-1] != '?') miscarr.push('?')
        ques = miscarr.join('');
        switch(cmd) {
            case 'welcome':
                bot.sendMessage({
                    to: '574963796406304784',
                    message: "Alright " + name + " Welcome to the server. Your basic roles have been assigned. You can check them by long pressing your profile pic on phone or clicking on your name on PC. You can ask for more roles in <#574964889945112586> and find reading material in <#584442250868817921>. You can now access the rest of the server."
                });
            break;
            case 'pollkick':
                bot.sendMessage({
                    to: channelID,
                    message: '@everyone should we kick ' + name + ' from the server? React with :thumbsup: for yes or :thumbsdown: for no'
                });
            break;
            case 'pollgulag':
                bot.sendMessage({
                    to: channelID,
                    message: '@everyone should we send ' + name + ' to gulag? React with :thumbsup: for yes or :thumbsdown: for no'
                });
            break;
            case 'pollmisc':
                bot.sendMessage({
                    to: channelID,
                    message: '@everyone ' + ques + ' React with :thumbsup: for yes or :thumbsdown: for no'
                });
            break;
            case 'pollhero':
                if (heroflag){
                    userid = name.match(/\d+/g)[0].trim()
                    bot.getUser({
                        userID: userid
                    }, (err, res) => {
                        if (err) console.log(err)
                        else {
                            let userObj = {};
                            for (let i=0; i<members.length; i++) {
                                if (res.id == members[i].user.id) {
                                    userObj = members[i]
                                    break;
                                }
                            }
                            if (userObj.roles.includes('574965703883620352') || userObj.roles.includes('574970037656616972') || userObj.roles.includes('612304645389549579') || userObj.roles.includes('610195397142642697') || userObj.roles.includes('574995934732222486') || userObj.roles.includes('574966182818349076')) {
                                bot.sendMessage({
                                    to: channelID,
                                    message: "You can't nominate someone from admin panel, KGB, bot or an Established Barbarian. Read the rules." 
                                });
                            }
                            else {
                                bot.sendMessage({
                                    to: herochannel,
                                    message: 'NOMINATION BY: ' + user + '\nNOMINEE: ' + name + '\nREASON: ' + reason + '\n' 
                                });
                            }
                        }
                    })
                }
                else{
                    bot.sendMessage({
                        to: channelID,
                        message: 'The poll is not open right now! LOL' 
                    });
                }
            break;
            case 'starthero':
                if (userID == '483125893137694731' || userID == '575372347486502923' || userID == '186374842197606400'){
                    heroflag = true;
                    bot.getMessages({
                        channelID: herochannel,
                        after: '645881674495950848'
                    }, (err, msgs) => {
                        if (err) console.log(err);
                        else {
                            msgs.forEach((msg) => {
                                bot.deleteMessage({
                                    channelID: herochannel,
                                    messageID: msg.id
                                })
                            });
                            bot.sendMessage({
                                to: herochannel,
                                message: '@everyone Hero of the Server poll is now open. You can start filing your nominations using my "=pollhero <mentioned-user> <reason>" command' 
                            });
                        }
                    })
                }
                else{
                    bot.sendMessage({
                        to: channelID,
                        message: 'Only admins can do that, cuck!' 
                    });
                }
            break;
            case 'stophero':
                if (userID == '483125893137694731' || userID == '575372347486502923' || userID == '186374842197606400'){
                    heroflag = false;
                    userid = name.match(/\d+/g)[0].trim()
                    bot.removeFromRole({
                        serverID: serverid,
                        roleID: heroid,
                        userID: hero
                    }, (err, res) => {
                        if (err) console.log(err)
                        else {
                            hero = userid;
                            bot.addToRole({
                                serverID: serverid,
                                roleID: heroid,
                                userID: userid
                            })
                        }
                    })
                    bot.getMessages({
                        channelID: herochannel,
                        after: '645881674495950848'
                    }, (err, msgs) => {
                        if (err) console.log(err);
                        else {
                            msgs.forEach((msg) => {
                                bot.deleteMessage({
                                    channelID: herochannel,
                                    messageID: msg.id
                                })
                            });
                            bot.sendMessage({
                                to: herochannel,
                                message: '@everyone The hero of the server polls are now closed. The Hero of the server for this week is ' + name + '. Congratulations :confetti_ball: :tada:' 
                            });
                        }
                    })
                }
                else{
                    bot.sendMessage({
                        to: channelID,
                        message: 'Only admins can do that, cuck!' 
                    });
                }
            break;
            case 'adore':
                bot.sendMessage({
                    to: channelID,
                    message: 'Thank you for making this server ' + name + '. Everyone hails you my lord. Bas khush?' 
                })
            break;
            case 'roast':
                roast = roasters[Math.floor(Math.random()*roasters.length)]
                // roast = roasters[8]
                userid = name.match(/\d+/g)[0].trim()
                // console.log(userid)
                if (userid == '575372347486502923'){
                    if (userID == '575372347486502923') {
                        bot.sendMessage({
                            to: channelID,
                            message: "No, my lord, I can't!" 
                        })
                    }
                    else if (userID == '483125893137694731' || userid == '186374842197606400'){
                        bot.sendMessage({
                            to: channelID,
                            message: "I don't roast my creator, who happens to be a councillor as well, nice try though!" 
                        })
                    }
                    else {
                        bot.sendMessage({
                            to: channelID,
                            message: "You really think I am gonna roast my creator, who happens to be a councillor as well? Nice try cuck!" 
                        })
                    }
                }
                else if (userid == '628153024136609792'){
                    if (userID == '483125893137694731' || userid == '186374842197606400' || userID == '575372347486502923'){
                        bot.sendMessage({
                            to: channelID,
                            message: "I don't roast myself!" 
                        })
                    }
                    else {
                        bot.sendMessage({
                            to: channelID,
                            message: "I don't roast myself, cuck!" 
                        })
                    }
                }
                else if (userid == '483125893137694731' || userid == '186374842197606400'){
                    bot.sendMessage({
                        to: channelID,
                        message: "Not gonna roast the councillors, eh!" 
                    })
                }
                else {
                    bot.sendMessage({
                        to: channelID,
                        message: name + roast
                    })
                }
            break;
            case 'gulag':
                if (userID == '483125893137694731' || userID == '575372347486502923' || userID == '186374842197606400' || userID == '609436979398049953'){
                    userid = name.match(/\d+/g)[0].trim()
                    content = reason.split(' ');
                    time = content[0];
                    unit = content [1];
                    crime = content.slice(2, content.length).join(' ')
                    members.forEach((member) => {
                        if(member.user.id == userid){
                            var user = {}
                            user.id = member.user.id;
                            user.removed = [];
                            bot.addToRole({
                                serverID: serverid,
                                roleID: gulagid,
                                userID: member.user.id
                            }, (err, res) => {
                                if(err) console.log(err)
                                else {
                                    console.log('Added Re-Education role')
                                    member.roles.forEach((role) => {
                                        if(roles.includes(role)) {
                                            user.removed.push(role)
                                            bot.removeFromRole({
                                                serverID: serverid,
                                                roleID: role,
                                                userID: member.user.id
                                            }, (err, res) => {
                                                if(err) console.log(err)
                                                else console.log('Removed - ' + role)
                                            })
                                        }
                                    })
                                    gulag.push(user);
                                    console.log(gulag);
                                    bot.sendMessage({
                                        to: gulagchannel,
                                        message: "INMATE: - " + name + "\nSENTENCE: " + time + " " + unit + "\nCRIME: " + crime
                                    }, (err, res) => {
                                        if(err) console.log(err)
                                        else console.log('Sent message to gulag') 
                                    })
                                }
                            })
                        }
                    })
                }
                else {
                    bot.sendMessage({
                        to: channelID,
                        message: "Nice try, you need to be a councillor or Leader of KGB to do that!"
                    })
                }
            break;
            case 'ungulag':
                if (userID == '483125893137694731' || userID == '575372347486502923' || userID == '186374842197606400' || userID == '609436979398049953'){
                    userid = name.match(/\d+/g)[0].trim()
                    gulag.forEach((inmate, index, object) => {
                        if(inmate.id==userid){
                            console.log('Found inmate');
                            inmate.removed.forEach((role) => {
                                bot.addToRole({
                                    serverID: serverid,
                                    roleID: role,
                                    userid: inmate.id
                                }, (err, res) => {
                                    if(err) console.log(err)
                                    else console.log('Added role - ' + role)
                                })
                            })
                            bot.removeFromRole({
                                serverID: serverid,
                                roleID: gulagid,
                                userid: inmate.id
                            }, (err, res) => {
                                if(err) console.log(err)
                                else object.splice(index, 1)
                            })
                        }
                    })
                    bot.sendMessage({
                        to: channelID,
                        message: name + " is back from gulag!"
                    })
                }
                else {
                    bot.sendMessage({
                        to: channelID,
                        message: "Nice try, you need to be a councillor or Leader of KGB to do that!"
                    })
                }
            break;
        }
     }
})